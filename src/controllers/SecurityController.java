package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;



import spring.web.dao.Portfolio;
import spring.web.dao.User;
import spring.web.services.usersService;

@Controller
public class SecurityController {

	private usersService usersService;
	
	@Autowired
	public void setUsersService(usersService usersService)
	{
		this.usersService= usersService; 
	}
		
	@RequestMapping("/login")
	public String ShowLogin()
	{
	    return "login";
	}
	
	@RequestMapping("/register")
	public String ShowRegister(Model model)
	{
		model.addAttribute("User", new User());
		
	    return "register";
	}
	
	
	@RequestMapping(value="/accountcreated", method=RequestMethod.POST)
	public String accountcreated(Model model, @Valid User user, BindingResult result)
	{	
		if(result.hasErrors())
		{	  
		  return "register";	  
		}    
		user.setAuthority("ROLE_USER");
		user.setEnabled(true);
		if(usersService.exists(user.getUsername()))
		{
			result.rejectValue("username", "DuplicateKey.user.username", "This username already exist!");
			return "register";
		}
		try
		{
		usersService.create(user);
		}
		catch (DuplicateKeyException e)
		{
		 result.rejectValue("username", "DuplicateKey.user.username", "This username already exist!");
		 return "register";
		}	
	    return "login";
	}
	
}
