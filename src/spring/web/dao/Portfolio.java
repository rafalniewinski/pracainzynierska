package spring.web.dao;



import java.util.Date;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;

import org.hibernate.validator.constraints.NotBlank;

public class Portfolio {

	private int idportfolio;
	
	private int idstock;
	
	private double balance;

	private String username;
	
	private String name;
	
	private Date date;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIdportfolio() {
		return idportfolio;
	}
	public void setIdportfolio(int idportfolio) {
		this.idportfolio = idportfolio;
	}
	public int getIdstock() {
		return idstock;
	}
	public void setIdstock(int idstock) {
		this.idstock = idstock;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public Portfolio() {

	}
	
	
	
	public Portfolio(int idportfolio, int idstock, double balance, String username, String name, Date date) {
		super();
		this.idportfolio = idportfolio;
		this.idstock = idstock;
		this.balance = balance;
		this.username = username;
		this.name = name;
		this.date = date;
	}
	
	public Portfolio(int idportfolio, int idstock, double balance, String username, String name) {
		super();
		this.idportfolio = idportfolio;
		this.idstock = idstock;
		this.balance = balance;
		this.username = username;
		this.name = name;
	}
	
		
}
