package spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import spring.web.dao.Stock;

@Component("StockDao")
public class StockDao {

private NamedParameterJdbcTemplate jdbc;
	
	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public StockDao()
    {  
		System.out.println("Jest Power Dzia�a !!!");
    }
	
	public List<Stock> ReturnAll()
	{		
//		List<String> supplierNames = new List<String>();
		List<Stock> nowa = new ArrayList<Stock>();
		   nowa = jdbc.query("SELECT * FROM stock",  new RowMapper<Stock>()  {
			   
			   public Stock mapRow(ResultSet rs, int rowNum) throws SQLException {
			   
				   Stock stock = new Stock();
				   stock.setIdstock(rs.getInt("idstock"));
				   stock.setName(rs.getString("name"));
				   stock.setPE(rs.getDouble("PE"));
				   stock.setPS(rs.getDouble("PS"));
				   stock.setPB(rs.getDouble("PB"));
				   stock.setROE(rs.getDouble("ROE"));
				   stock.setROA(rs.getDouble("ROA"));
			   
				   return stock;
			   }
		   });
		  
		return nowa;
	}
	
	
	public List<Integer> ReturnAllByName(List<String> stocki)
	{
		List<Integer> nowa = new ArrayList<Integer>();
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		
		
		for(int i=0; i<stocki.size(); i++)
		{
		params.addValue("name",  stocki.get(i));	
		int liczba;
		liczba=jdbc.queryForObject("select idstock from stock where name=:name", params, Integer.class);
		nowa.add(liczba);
	    System.out.println(liczba);
		}		
		return nowa;
	}
	
	public Stock ReturnOneStockByName(String name)
	{
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("name", name);
		
		return jdbc.queryForObject("select * from stock where name=:name", params,
				new RowMapper<Stock>() {

			public Stock mapRow(ResultSet rs, int rowNum)
					throws SQLException {

                Stock stock = new Stock();
				stock.setIdstock(rs.getInt("idstock"));
				stock.setName( rs.getString("name"));
				stock.setPE(rs.getDouble("PE"));
                stock.setPS(rs.getDouble("PS"));
                stock.setPB(rs.getDouble("PB"));
                stock.setROE(rs.getDouble("ROE"));
                stock.setROA(rs.getDouble("ROA"));
				return stock;
			}
		});	
	}
	
	public Stock ReturnsOneStockById(int id)
	{
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("idstock", id);
		
		return jdbc.queryForObject("select * from stock where idstock=:idstock", params,
				new RowMapper<Stock>() {

			public Stock mapRow(ResultSet rs, int rowNum)
					throws SQLException {

                Stock stock = new Stock();
				stock.setIdstock(rs.getInt("idstock"));
				stock.setName( rs.getString("name"));
				stock.setPE(rs.getDouble("PE"));
                stock.setPS(rs.getDouble("PS"));
                stock.setPB(rs.getDouble("PB"));
                stock.setROE(rs.getDouble("ROE"));
                stock.setROA(rs.getDouble("ROA"));
				return stock;
			}
		});	
	}
	
public boolean create(Stock stock) {
		
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(stock);
		
		return jdbc.update("insert into stock (name, PE, PS,PB,ROE,ROA) values (:name, :PE, :PS, :PB, :ROE, :ROA)", params) == 1;
	}

public boolean update(Stock stock) {
	
	BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(stock);
	 System.out.println("trolo");
	 System.out.print(stock.getIdstock());
	 System.out.print(stock.getName());
	return jdbc.update("update stock set name=:name, PE=:PE, PS=:PS, PB=:PB, ROE=:ROE, ROA=:ROA where idstock=:idstock", params) == 1;
//	 System.out.println("udalo sie ");
}
	
	
}
