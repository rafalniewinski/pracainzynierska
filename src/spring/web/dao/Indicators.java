package spring.web.dao;

public class Indicators {

	private double EMA;
	private double MACD;
	private double ROC;
	private String SignalEma;
	private String SignalMACD;
	private String SignalROC;
	
	public Indicators() {

	}
	
	public Indicators(double eMA, double mACD, double rOC, String signalEma, String signalMACD, String signalROC) {
		super();
		EMA = eMA;
		MACD = mACD;
		ROC = rOC;
		SignalEma = signalEma;
		SignalMACD = signalMACD;
		SignalROC = signalROC;
	}
	
	public String getSignalEma() {
		return SignalEma;
	}
	public void setSignalEma(String signalEma) {
		SignalEma = signalEma;
	}
	public String getSignalMACD() {
		return SignalMACD;
	}
	public void setSignalMACD(String signalMACD) {
		SignalMACD = signalMACD;
	}
	public String getSignalROC() {
		return SignalROC;
	}
	public void setSignalROC(String signalROC) {
		SignalROC = signalROC;
	}
	public double getEMA() {
		return EMA;
	}
	public void setEMA(double eMA) {
		EMA = eMA;
	}
	public double getMACD() {
		return MACD;
	}
	public void setMACD(double mACD) {
		MACD = mACD;
	}
	public double getROC() {
		return ROC;
	}
	public void setROC(double rOC) {
		ROC = rOC;
	}
}
