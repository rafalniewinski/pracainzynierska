package spring.web.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.web.dao.Portfolio;
import spring.web.dao.PortfolioDao;

@Service("PortfolioService")
public class PortfolioService {

	PortfolioDao portfoliodao;
	
	@Autowired
	public void setPortfolioDao(PortfolioDao portfoliodao)
	{
		this.portfoliodao= portfoliodao;
	}
	
	public void addPortfolio(Portfolio portfolio)
	{
		 portfoliodao.create(portfolio);
	}
	
	public List<Portfolio> getPortfoilos(String username)
	{
       return portfoliodao.ReturnAllPortfolioByUserName(username);
	}
	
	public boolean delete(int id)
	{
       return portfoliodao.delete(id);
	}
	
	public boolean deleteByIdStock(int id)
	{
		return portfoliodao.deleteByStockId(id);
	}
	
	public Portfolio ReturnOnePortfolioWhereIdandUsername(String name, int idstock)
	{
		return portfoliodao.ReturnOnePortfolioWhereIdandUsername(name, idstock);
	}
	
}
