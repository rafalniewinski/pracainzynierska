package spring.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.web.dao.User;
import spring.web.dao.UserDao;

@Service("usersService")
public class usersService {

	private UserDao usersDao; 
	
    @Autowired
	public void setUsersDao(UserDao usersDao) {
		this.usersDao = usersDao;
	}

	public void create(User user) {
		usersDao.create(user);
	}

	public boolean exists(String username) {
		return usersDao.exists(username);
	}
	
}
