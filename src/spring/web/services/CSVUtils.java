package spring.web.services;


import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.springframework.stereotype.Service;


@Service("CSVUtils")
public class CSVUtils {

	 private static final char DEFAULT_SEPARATOR = ' ';

	    public static void writeLine(Writer w, List<String> values, boolean first) throws IOException {
	        writeLine(w, values, DEFAULT_SEPARATOR , ' ', first );
	    }

	    public static void writeLine(Writer w, List<String> values, char separators) throws IOException {
	        writeLine(w, values, separators , ' ', true);
	    }

	    //https://tools.ietf.org/html/rfc4180
	    private static String followCVSformat(String value) {

	        String result = value;
	        if (result.contains("\"")) {
	            result = result.replace("\"", "\"\"");
	        }
	        return result;

	    }

	    public static void writeLine(Writer w, List<String> values, char separators, char customQuote, boolean first) throws IOException {

	        //default customQuote is empty

	        if (separators == ' ') {
	            separators = DEFAULT_SEPARATOR;
	        }

	        StringBuilder sb = new StringBuilder();
	        for (String value : values) {
	            if (!first) {
	          //      System.out.println(value);
	                sb.append(separators);
	            }
	            if (customQuote == ' ') {
	               
	           //     System.out.println(value);
	                sb.append(followCVSformat(value));
	                if(!first)
	                {
	             //   System.out.println(value);
	                sb.append("\n");
	                }
	            } else {
	          //      System.out.println(value);
	                sb.append(customQuote).append(followCVSformat(value)).append(customQuote);
	            }
	        }
	        sb.append("\n");
	   
	       // System.out.println("to bedzie zapisane");
	       // System.out.println(sb.toString());
            w.append(sb.toString());
          //  System.out.println("zapisa�o ? ");

	    }	
}
