<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>


<link href="${pageContext.request.contextPath}/static/css/portfolio.css" rel="stylesheet" type="text/css">
<script src="${pageContext.request.contextPath}/static/javascript/jquery-3.2.1.min.js"></script>
<script src="${pageContext.request.contextPath}/static/javascript/portfolio.js"></script>

<link href="${pageContext.request.contextPath}/static/css/navbar.css" rel="stylesheet" type="text/css">

<link href="${pageContext.request.contextPath}/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
</head>
<body>
<h1> Dodaj Akcje do Portfela ! </h1>

</br>
</br>
<form id="form" name="form" action="${pageContext.request.contextPath}/dashboard/portfolio/create"  method="post" >

<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
<tr><input type="text" id="stock-name" name="stock-name" placeholder="Nazwa akcji"   onblur="this.placeholder='Nazwa akcji'" onfocus="this.placeholder=''"/> <input type="text" id="stock-wage" name="stock-wage" placeholder="Waga akcji" onblur="this.placeholder='Waga akcji'"  onfocus="this.placeholder=''" /> <input id="date" type="date" name="date" /> <div name="add" class="button-alpha" ><i class="fa fa-plus" aria-hidden="true"></i></div><div name="add" class="button-beta" ><i class="fa fa-minus" aria-hidden="true"></i></div><button type="submit" class="btn btn-primary"  value="Dodaj Portfel"/>Dodaj Portfel </button></tr> 
</form> 
<script>
$(document).ready(function() {
	var now = new Date();
	
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);

	var today = now.getFullYear()+"-"+(month)+"-"+(day);

	$('#date').val(today);
});

</script>

</body>
</html>