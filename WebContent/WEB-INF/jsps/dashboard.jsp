<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

<link href="${pageContext.request.contextPath}/static/css/navbar.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/static/css/dashboard.css" rel="stylesheet" type="text/css">
</head>
<body>

<jsp:include page="navbar.jsp"/>


<div class="Main-Panel">
 <div class="center"> Twój Portfel <div class="row-right2">   <a href="${pageContext.request.contextPath}/dashboard/portfolio">  <button type="button" class="btn btn-info">Stwórz Portfel</button></a>    <a href="${pageContext.request.contextPath}/dashboard/portfolio/update">  <button type="button" class="btn btn-warning">Zaktualizuj Portfel</button> </a>    </div></div>
 <div class="center2"> <tr>Stopa Zwrotu z całego portfela</tr> <div class="row-right">   <c:out value="${StopyZwrotu} "></c:out>% </div></div> 
 <div class="center2"> <tr>Współczynnik Beta dla całego portfela</tr> <div class="row-right">    <c:out value="${sumabety}"></c:out> </div></div> 

   
 </div>

  
  <c:forEach var="stock" items="${stocks}">

 <div class="center"> <tr><h1><c:out value="${stock.name}"></c:out></h1></tr> </div>

 <div class="indicators">
  <div class="box">
     <tr>Analiza Finansowa Spółki</tr></br></br><tr>PE: <div class="row-right">  <c:out value="${stock.PE}"></c:out> </div> </tr></br><tr>PS: <div class="row-right"> <c:out value="${stock.PS}"></c:out></div></tr></br><tr>PB:<div class="row-right"> <c:out value="${stock.PB}"></c:out> </div></tr></br><tr>ROE: <div class="row-right"> <c:out value="${stock.ROE}"></c:out> </div> </tr></br><tr>ROA:<div class="row-right"> <c:out value="${stock.ROA}"></c:out></div></br></tr><tr> Beta:<div class="row-right"> <c:out value="${stock.beta}"></c:out> </div></tr>     
  </div>
  
    <div class="box">
     <tr>Stopy Zwrotu</tr></br></br>1M: <div class="row-right"> <c:out value="${stock.returns.m1}"></c:out>%</div></br>3M:<div class="row-right">  <c:out value="${stock.returns.m3}"></c:out>%</div></br>6M:<div class="row-right"><c:out value="${stock.returns.m6}"></c:out>%</div></br>12M:<div class="row-right"><c:out value="${stock.returns.m12}"></c:out>%</div></br>
  </div>

  <div class="box">
     <tr>Sygnały AT</tr></br></br>EMA(21): <div class="row-right">  <c:out value="${stock.indicatots.signalEma}"></c:out></div></br>MACD(21,14): <div class="row-right">  <c:out value="${stock.indicatots.signalMACD}"></c:out></div></br>ROC(21):<div class="row-right">  <c:out value="${stock.indicatots.signalROC}"></c:out></div></br>
  </div>
</div>

 </c:forEach>








</body>
</html>