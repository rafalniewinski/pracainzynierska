<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page import = "java.io.*,java.util.*,java.sql.*"%>
<%@ page import = "javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix = "c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix = "sql"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<link href="${pageContext.request.contextPath}/static/css/portfolio.css" rel="stylesheet" type="text/css">
<script src="${pageContext.request.contextPath}/static/javascript/jquery-3.2.1.min.js"></script>
<script src="${pageContext.request.contextPath}/static/javascript/portfolio2.js"></script>
<script src="${pageContext.request.contextPath}/static/javascript/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<link href="${pageContext.request.contextPath}/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/static/css/dashboard.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/static/css/home.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/static/css/navbar.css" rel="stylesheet" type="text/css">
</head>
<body>




<jsp:include page="navbar.jsp"/>


<div class="main-text"> Zaktualizuj Portfel   </div>

<div class="montage">

<form id="form2" name="form2" action="${pageContext.request.contextPath}/dashboard/portfolio/update/action" onsubmit="return validateForm2()"  method="post" commandName="portfolio">
<sf:errors path="text" Class="error">   </sf:errors>
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
 
<div class="buttons">
<div name="add" class="button-alpha" ><i class="fa fa-plus" aria-hidden="true"></i></div><div name="add" class="button-beta" ><i class="fa fa-minus" aria-hidden="true"></i></div><input type="submit" class="btn btn-primary"  value="Update Portfel"/>
</div>

<div class="portfolios">
  <c:forEach var="portfolio" items="${portfolios}" varStatus="count">
  <tr>
  <input type="text" id="stock-name-${count.count}" name="stock-name-${count.count}" class="stock-name" placeholder="Nazwa akcji"  onblur="this.placeholder='Nazwa akcji'" onfocus="this.placeholder=''"  value="${portfolio.name}"/>
  <input type="text" id="stock-wage-${count.count}" name="stock-wage-${count.count}" placeholder="Waga akcji" onblur="this.placeholder='Waga akcji'" value="${portfolio.balance}" onfocus="this.placeholder=''" /> 
  <input id="date-${count.count}" type="date" name="date-${count.count}" value="${portfolio.date}" />
 </tr>
 </br>

 </c:forEach>



</form> 
</div>
</div>
</body>
</html>