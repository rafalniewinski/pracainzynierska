<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
      <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/static/css/home.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/static/css/navbar.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/static/css/paneladmin.css" rel="stylesheet" type="text/css">
    <script src="${pageContext.request.contextPath}/static/javascript/script.js"></script>
<link href="${pageContext.request.contextPath}/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<script src="${pageContext.request.contextPath}/static/javascript/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


</head>
<body>
<div class="buttony">
   <tr> <th> <a href="${pageContext.request.contextPath}/admin/panel/stockcreat">  <button type="button" class="btn btn-info">Dodaj Spółke</button></a></th> <th><a href="${pageContext.request.contextPath}/admin/getstock">  <button type="button" class="btn btn-info">Pobierz Dane</button></a>   </th> <th><a href="${pageContext.request.contextPath}/admin/updatestock">  <button type="button" class="btn btn-info">Zaktualizuj Dane</button></a></th>    </tr>
 </div>
<jsp:include page="navbar.jsp"/>
 <div class="panel-admin-dashboard">
 
 <tr><th>Id spółki</th> <th>Nazwa spółki</th></tr> </br></br>

  <c:forEach var="stock" items="${stocks}">


  <div class="box">
     <tr><th><c:out value="${stock.idstock}"></c:out></th> <th><c:out value="${stock.name}"></c:out></th><th><div class="row-right"><a href="${pageContext.request.contextPath}/admin/panel/stockupdate/${stock.idstock}"> <i class="fa fa-pencil" aria-hidden="true" style="padding-top: 10px;"></i></a>  </div></th>  </tr>
  </div>

 </c:forEach>
</div>


</body>
</html>