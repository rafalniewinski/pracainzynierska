<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
 <meta name="_csrf" content="${_csrf.token}"/>
  <meta name="_csrf_header" content="${_csrf.headerName}"/>
<link href="${pageContext.request.contextPath}/static/css/home.css" rel="stylesheet" type="text/css">
<script src="${pageContext.request.contextPath}/static/javascript/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />


	<script>
$(document).ready(function() {
		  
//	  $.ajaxSetup
 //     (
    //     {
       //      cache:false,
     //        beforeSend: function (xhr) { xhr.setRequestHeader('Authorization', token); }
    //     }
    //  );
	
	      var token =  $('meta[name="_csrf"]').attr("content");
		  var header = $("meta[name='_csrf_header']").attr("content");
		  alert(token);
		   $(document).ajaxSend(function(e, xhr, options) {
		    xhr.setRequestHeader('X-CSRF-Token',  token);
		   });
	
	

	$("#w-input-search").autocomplete({
	source: function(request, response) {
	$.ajax({
	url: "${pageContext.request.contextPath}/test/action",
	type: "POST",
	data: { term: request.term },
	 
	dataType: "json",
	
	success: function(data) {
	response($.map(data, function(v,i){
	return {
	label: v.name,
	value: v.name
	};
	}));
	}
	});
	}
	});

	
	
	});
	</script>


</head>
<body>
	<h2>Spring MVC + jQuery + Autocomplete example</h2>

	 <form class="form-style">
     <div class="ui-widget">
		<input type="text"  id="w-input-search" />
		
		<span>
			<button id="w-button-search" type="button">Search</button>
		</span>
	 </div>
 </form>
		
</body>
</html>