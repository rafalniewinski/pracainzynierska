<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
<script src="${pageContext.request.contextPath}/static/javascript/jquery-3.2.1.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; UTF-8">
<link href="${pageContext.request.contextPath}/static/css/navbar.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="${pageContext.request.contextPath}/static/css/stock.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<title>Insert title here</title>



</head>
<body>

<jsp:include page="navbar.jsp"/>

<div class="chart">
<div id="plotly-div"></div>
</div>
  
</body>

<script src="${pageContext.request.contextPath}/static/javascript/chart.js"></script>
</br>
</br>

<div class="Absolute">
 <div class="center"> <tr><h1><c:out value="${stock.name}"></c:out></h1> </tr> </div>
 
 <div class="indicators">
  <div class="box">
     <tr>Analiza Finansowa Spółki</tr></br></br><tr>C/Z: <div class="row-right"> <c:out value="${stock.PE}"></c:out> <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" title="C/Z - Cena do zysku" data-content="Wskaźnik wyrażający atrakcyjność akcji względem zysków osiąganych przez spółkę im mniejsza wartość tego wskaźnika tym mniej musimy zapłacić za akcje w stosunku do zysków osiąganych przez te spółkę, wskaźnik c/z może również służyć jako ocena wartości całego rynku wartość tego wskaźniku powyżej dwudziestu informuje o przewartościowaniu rynku w tej sytuacji inwestorzy pokładają wysokie nadzieje co do przyszłych zysków spółki." style="cursor:pointer" ></i> </div></tr>
     </br><tr>C/S: <div class="row-right "><c:out value="${stock.PS}"></c:out> <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" title="C/S - Cena do sprzedaży" data-content="Wskaźnik informujący jak się ma wartość akcji w stosunku do wartości zysków z sprzedaży, im mniejsza wartość tego wskaźnika tym spółka jest bardziej atrakcyjna, dodatkowo wskaźnik może być dobrym filtrem badania efektywności zarządu spółki ponieważ spółka może osiągnąć większe zyski przez manipulacje podatkowe a nie z samego działa spółki.  " style="cursor:pointer" ></i></div></tr>
     </br><tr>PB: <div class="row-right"> <c:out value="${stock.PB}"></c:out> <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" title="C/WK - Cena do Wartości księgowej" style="cursor:pointer" data-content="Wskaźnik wyceny aktywów danej spółki.  wartość poniżej jedynki oznacza że kupując akcje spółki zyskujemy prawo do aktywów o wartości przypadających na jedną akcje większą od ceny jaką musi zapłacić za akcje, zatem wartość tego wskaźnika poniżej jedynki informuje o niedowartościowaniu spółki zaś wartość wskaźnika powyżej trzech mówi o przewartościowaniu spółki. "></i> </div> </tr>
     </br><tr>ROE: <div class="row-right"> <c:out value="${stock.ROE}"></c:out> <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" title="ROE" style="cursor:pointer" data-content="Wskaźnik informujący o zyskach netto spółki w stosunku do kapitał własnego ( aktywa – pasywa) im większa wartość tego wskaźnika tym spółka jest lepiej zarządzana. "></i></div></tr>
     </br><tr>ROA: <div class="row-right"> <c:out value="${stock.ROA}"></c:out> <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" title="ROA" style="cursor:pointer" data-content="Wskaźnik informujący o zyskach netto spółki w stosunku do Aktyw ogółem  im większa wartość tego wskaźnika tym lepiej całość aktywów spółki pracuje na jej zyski w przeciwieństwie do ROE uwzględnia też kapitał który został pożyczony. "></i></div></br></tr>
  </div>
  
    <div class="box">
     <tr>Stopy Zwrotu</tr></br></br>1M: <div class="row-right"> <c:out value="${c1}"></c:out>% </div></br>3M:<div class="row-right"> <c:out value="${c2}"></c:out>%</div></br>6M: <div class="row-right"><c:out value="${c3}"></c:out>%</div></br>12M:<div class="row-right"> <c:out value="${c4}"></c:out>%</div></br>
  </div>

  <div class="box">
     <tr>Sygnały AT</tr></br></br>EMA(21): <div class="row-right"> <c:out value="${EMAsygnał}"></c:out> <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" title="EMA - ( Exponential Moving Averange )" style="cursor:pointer" data-content="Wykładnicza średnia ruchoma w przeciwieństwie to zwykłej średniej ruchomej nadaje większe znaczenie cenom notowań bardziej aktualnych przypisując jednocześnie coraz dalszym notowaniom mniejsze wagi sygnały kupna generowany przez ten wskaźnik występuję w momencie kiedy wskaźnik przecina aktualną cenę od dołu zaś moment przecięcia wskaźnika od góry z aktualną ceną waloru jest sygnałem sprzedaży  "></i></div>
     </br>MACD(21,14): <div class="row-right"> <c:out value="${MACDsyngał}"></c:out> <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" title="MACD - (  Moving Auerage Convergen.ce Diuergence  ) " style="cursor:pointer" data-content="Wskaźnik opierający się na dwóch średnich ważonych z różnymi krokami uśrednienia zaś sam wskaźnik opiera się na ich różnicy tych średnich. sygnał kupna generowany jest w momencie kiedy krótsza średnia ważona przecina dłuższą od dołu zaś sygnał sprzedaży w momencia przecięcia średniej dłużej przez krótszą od góry.  "></i></div>
     </br>ROC(21):<div class="row-right"> <c:out value="${ROC21}"></c:out> <i class="fa fa-question-circle" aria-hidden="true"  data-toggle="popover" title="ROC - ( Rate of Change ) " style="cursor:pointer" data-content="Wskaźnik informujący o dynamice ceny danego waloru a jego sama wartość mówimy nam jak bardzo zmieniły się ceny w stosunku do k sesji wcześniejszych i tak dla przykładu jeżeli cena aktualnego waloru wynosi 120 zł a 5 sesji wcześniej wynosiła 100 zł to wskaźnik ROC zanotuje poziom 20%. Sygnały kupna z tego wskaźnika występuje na  poziomie -20% mówi się wtedy że rynek jest wyprzedany zaś poziom 20% to wykupienie rynku co z kolei jest sygnałem sprzedaży poziom w okolicach 0 jest poziomem neutralnym.  "></i></div>
     </br>
  </div>
</div>
 
</div>


<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
});
</script>


</html>