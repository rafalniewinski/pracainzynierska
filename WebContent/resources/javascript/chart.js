
  

$(document).ready(function(){

	    var d1 = [];
	    var op = [];
	    var hi = [];
	    var lo = [];
	    var cl = [];
	    var time;
	    var pathname = window.location.pathname; // Returns path only
	    var res =  pathname.slice(12,22);
	    
	    var xxx =pathname.replace("/Inzynierka/stock","");

  $.ajax({
	  
   url:"http://localhost:8080/Inzynierka/static/stock"+xxx+".csv",
   dataType:"text",
   success:function(data)
   {
	   
    var employee_data = data.split(/\r?\n|\r/);
    var table_data = '<table class="table table-bordered table-striped">';
    var nextgen = '<table class="table table-bordered table-striped">';
    
    var date = [];
    var open2 = [];
    var highest = [];
    var lowest = [];
    var close2 = [];
    
    var licznikdata=0;
    var licznikopen=0;
    var licznikhighest=0;
    var liczniklowest=0;
    var licznikclose=0;
    
    for(var count = 0; count<employee_data.length; count++)
    {
     var cell_data = employee_data[count].split(";");
     table_data += '<tr>';
     for(var cell_count=0; cell_count<cell_data.length; cell_count++)
     {
      if(count === 0)
      {
       table_data += '<th>'+cell_data[cell_count]+'</th>';
      }
      else
      {
        if(cell_count==0)
        {
          date[licznikdata] = cell_data[cell_count];
          licznikdata++;
        }
        if(cell_count==1)
        {
          open2[licznikopen] = cell_data[cell_count];
          licznikopen++;
        }
        if(cell_count==2)
        {
          highest[licznikhighest] = cell_data[cell_count];
          licznikhighest++;
        }
        if(cell_count==3)
        {
          lowest[liczniklowest] = cell_data[cell_count];
          liczniklowest++;
        }
        if(cell_count==4)
        {
          close2[licznikclose] = cell_data[cell_count];
          licznikclose++;
        }
        
       table_data += '<td>'+cell_data[cell_count]+'</td>';
      }
     }
     table_data += '</tr>';
    }

    table_data += '</table>';

    d1 = date;
    op = open2;
    hi = highest;
    lo = close2;
    cl = lowest;
    time = licznikdata-1;
    

    var trace1 = {
    		
    		  x:d1 , 
    		  
    		  close:lo , 
    		  
    		  decreasing: {line: {color: '#7F7F7F'}}, 
    		  
    		  high:hi , 
    		  
    		  increasing: {line: {color: '#17BECF'}}, 
    		  
    		  line: {color: 'rgba(31,119,180,1)'}, 
    		  
    		  low:cl , 
    		  
    		  open:op , 
    		  
    		  type: 'candlestick', 
    		  xaxis: 'x', 
    		  yaxis: 'y'
    		};
          
         
    		var data = [trace1];

    		var layout = {
    		  dragmode: 'zoom', 
    		  margin: {
    		    r: 10, 
    		    t: 25, 
    		    b: 40, 
    		    l: 60
    		  }, 
    		  showlegend: false, 
    		  xaxis: {
    		    autorange: true, 
    		    rangeslider: {range: [ d1[20], d1[time]]}, 
    		    title: 'Date', 
    		    type: 'date'
    		  }, 
    		  yaxis: {
    		    autorange: true, 
    		    type: 'linear'
    		  },
    		  
    		  annotations: [
    		    {
    		      x: d1[20],
    		      y: 0.9,
    		      xref: 'x',
    		      yref: 'paper',
    		      text: 'largest movement',
    		      font: {color: 'magenta'},
    		      showarrow: true,
    		      xanchor: 'right',
    		      ax: -20,
    		      ay: 0
    		    }
    		  ],
    		  
    		  shapes: [
    		      {
    		          type: 'rect',
    		          xref: 'x',
    		          yref: 'paper',
    		          x0: d1[20],
    		          y0: 0,
    		          x1: d1[time],
    		          y1: 1,
    		          fillcolor: '#d3d3d3',
    		          opacity: 0.2,
    		          line: {
    		              width: 0
    		          }
    		      }
    		    ]
    		};		  		   
    		
    		Plotly.plot('plotly-div', data, layout);
    
   }

  
  });
  
});
